﻿
$ErrorActionPreference = 'Stop'; 


$packageName= 'alchemy'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://depot.alchemyviewer.org/pub/windows/bin/Alchemy_5_0_7_41341_i686_Setup.exe' 
$url64      = 'https://depot.alchemyviewer.org/pub/windows64/bin/Alchemy_5_0_7_41341_x86_64_Setup.exe' 

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64

  silentArgs    = "/S" 
  validExitCodes= @(0, 3010, 1641)

  softwareName  = 'alchemy*'
  checksum      = '87fa21778888208e11a062109bf5ec20fb46b7e58685c0af51de530ebdbbcf35'
  checksumType  = 'sha256' 
  checksum64    = '7aa8de770e579f0fec9834cda5a2cb2c27dbec2a1047dd18d9a87686e79cbd00'
  checksumType64= 'sha256' 
}

Install-ChocolateyPackage @packageArgs