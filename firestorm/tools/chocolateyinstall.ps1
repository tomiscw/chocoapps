﻿
$ErrorActionPreference = 'Stop'; 


$packageName= 'firestorm' 
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'http://downloads.firestormviewer.org/windows/Phoenix-FirestormOS-Release-5-0-11-53634_Setup.exe'
$url64      = 'http://downloads.firestormviewer.org/windows/Phoenix-FirestormOS-Releasex64-5-0-11-53634_Setup.exe' 

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'exe' 
  url           = $url
  url64bit      = $url64

  silentArgs    = "/S"
  validExitCodes= @(0, 3010, 1641)

  softwareName  = 'firestorm*'
  checksum      = '154FC28D554F8B972563B2E4ADB54A131C928D08'
  checksumType  = 'sha1' 
  checksum64    = '92DD6CCE6117A9BEFAAFB244F5288A925C060E34'
  checksumType64= 'sha1' 
}

Install-ChocolateyPackage @packageArgs 