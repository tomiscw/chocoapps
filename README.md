# My Chocolatey Repository

My Chocolatey Repository is hub for applications to be distrubated in Chocolatey package format based on NuGet.

## What is Chocolatey?
Chocolatey is a package manager for Windows (like apt-get or yum but for Windows). It was designed to be a decentralized framework for quickly installing applications and tools that you need. It is built on the NuGet infrastructure currently using PowerShell as its focus for delivering packages from the distros to your door, err computer.

# FAQ
## How do I build a package?
It's surprisingly simple. Assuming Chocolatey is installed, type in `cpack [package].nuspec` from the command line and pack it all.

## Do you have your own repository?

Yes. You can check it out at [Bintray](https://bintray.com/tomascw/choco) or add it to your Chocolatey installation with the following command: ``choco source add -n [name] -s https://api.bintray.com/nuget/tomascw/choco``.